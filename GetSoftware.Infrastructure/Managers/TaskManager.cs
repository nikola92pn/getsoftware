﻿using TaskDao = GetSoftware.DAL.Models.Task;
using Task = GetSoftware.Infrastructure.Models.Task;
using User = GetSoftware.Infrastructure.Models.User;
using GetSoftware.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System;
using GetSoftware.DAL.Models;
using GetSoftware.Infrastructure.Models;

namespace GetSoftware.Infrastructure.Managers
{
    public class TaskManager
    {
        private readonly IRepository<TaskDao> repository;

        #region Ctor

        /// <summary>
        /// Creates an instance of TaskManager
        /// </summary>
        /// <param name="_repository"></param>
        public TaskManager(IRepository<TaskDao> _repository)
        {
            repository = _repository;
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Gets all tasks
        /// </summary>
        /// <returns></returns>
        public List<Task> GetAllTasks()
        {
            List<TaskDao> tasks = repository.GetAll();

            return tasks.Select(t => new Task(t)).ToList();
        }

        /// <summary>
        /// Gets task by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task GetTaskById(int id)
        {
            TaskDao task = repository.GetById(id);

            return task != null ? new Task(task) : null;
        }

        /// <summary>
        /// Gets tasks for user
        /// </summary>
        public Task GetTaskForEditing(int taskId, User forUser)
        {
            Task task = GetTaskById(taskId);

            if (CheckTaskPersmissionForUser(forUser, task))
            {
                return null;
            }

            return task;
        }
        
        public bool UpdateTaks(Task task)
        {
            TaskDao taskDao = MapTaskModelToDao(task);

            return repository.Edit(taskDao);
        }

        public bool InsertTask(Task task)
        {
            TaskDao taskDao = MapTaskModelToDao(task);

            return repository.Add(taskDao);
        }

        public bool RemoveTask(int id)
        {
            return repository.Remove(id);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Maps Task model to DAO object
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        private TaskDao MapTaskModelToDao(Task task)
        {
            return new TaskDao
            {
                Id = task.Id,
                AssigneeId = task.Assingee?.Id,
                Deadline = DateTime.ParseExact(task.Deadline, "yyyy-MM-ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture),
                Description = task.Description,
                Progress = task.Progress,
                ProjectId = task.Project.Id,
                Status = (int)task.Status
            };
        }

        /// <summary>
        /// Checks does user can edit task
        /// </summary>
        /// <param name="forUser"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        private static bool CheckTaskPersmissionForUser(User forUser, Task task)
        {
            if (forUser == null || task == null)
            {
                return false;
            }

            return (forUser.Role == RoleType.DEVELOPER && forUser.Id != task.Assingee?.Id);
        }
        #endregion
    }
}
