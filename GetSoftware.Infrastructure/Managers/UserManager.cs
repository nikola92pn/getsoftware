﻿using GetSoftware.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using UserDao = GetSoftware.DAL.Models.User;
using TaskDao = GetSoftware.DAL.Models.Task;
using Task = GetSoftware.Infrastructure.Models.Task;
using User = GetSoftware.Infrastructure.Models.User;
using GetSoftware.Infrastructure.Models;

namespace GetSoftware.Infrastructure.Managers
{
    public class UserManager
    {

        private readonly IRepository<UserDao> userRepository;
        private readonly IRepository<TaskDao> taskRepository;

        #region Ctor

        /// <summary>
        /// Creates an instance of UserManager
        /// </summary>
        /// <param name="_userRepository"></param>
        /// <param name="_taskRepository"></param>
        public UserManager(IRepository<UserDao> _userRepository, IRepository<TaskDao> _taskRepository)
        {
            userRepository = _userRepository;
            taskRepository = _taskRepository;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Gets all project managers
        /// </summary>
        /// <returns></returns>
        public List<User> GetAllProjectManagers()
        {
            return GetAllUsers().Where(u => u.Role == RoleType.PROJECT_MANAGER).ToList();
        }

        /// <summary>
        /// Gets user by username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User GetUser(string username, string password)
        {
            List<UserDao> users = userRepository.GetAll();

            UserDao result = users.FirstOrDefault(u => u.Username == username && u.Password == password);
            
            return result != null ? new User(result) : null;
        }

        /// <summary>
        /// Gets all avalable users for assign
        /// </summary>
        /// <returns></returns>
        public List<User> GetAllAvailableUsers()
        {
            List<User> users = GetAllUsers();
            List<User> developers = users.Where(u => u.Role == RoleType.DEVELOPER).ToList();

            TaskManager taskMng = new TaskManager(taskRepository);
            List<Task> tasks = taskMng.GetAllTasks();

            List<int> busyDevIds = GetBusyDeveloperIds(developers, tasks);

            users.RemoveAll(d => busyDevIds.Contains(d.Id));

            return users;
        }

        /// <summary>
        /// Gets all avalilable developers for assign
        /// </summary>
        /// <returns></returns>
        public List<User> GetAllAvailableDevelopers()
        {
            List<User> developers = GetAllUsers().Where(u => u.Role == RoleType.DEVELOPER).ToList();

            TaskManager taskMng = new TaskManager(taskRepository);
            List<Task> tasks = taskMng.GetAllTasks();

            List<int> busyDevIds = GetBusyDeveloperIds(developers, tasks);

            developers.RemoveAll(d => busyDevIds.Contains(d.Id));

            return developers;
        }

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns></returns>
        public List<User> GetAllUsers()
        {
            List<UserDao> users = userRepository.GetAll();

            return users.Select(u => new User(u)).ToList();
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserById(int id)
        {
            UserDao user = userRepository.GetById(id);

            return user != null ? new User(user) : null;
        }

        /// <summary>
        /// Updates an instance of user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UpdateUser(User user)
        {
            if (user.Password == null)
            {
                user.Password = GetUserById(user.Id).Password;
            }

            UserDao userDao = MapUserModelToDao(user);

            return userRepository.Edit(userDao);
        }

        /// <summary>
        /// Insertes an instance of user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool InsertUser(User user)
        {
            if (user.Password == null)
            {
                return false;
            }

            UserDao userDao = MapUserModelToDao(user);

            return userRepository.Add(userDao);
        }

        /// <summary>
        /// Removes an instance of user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool RemoveUser(int id)
        {
            return userRepository.Remove(id);
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Gets busy developers list of ids
        /// </summary>
        /// <param name="developers"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        private static List<int> GetBusyDeveloperIds(List<User> developers, List<Task> tasks)
        {
            List<int> busyDevIds = new List<int>();

            foreach (User dev in developers)
            {
                if (tasks.Where(t => t.Assingee?.Id == dev.Id).Count() >= 3)
                {
                    busyDevIds.Add(dev.Id);
                }
            }

            return busyDevIds;
        }
        
        /// <summary>
        /// Maps User model to DAO object
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private UserDao MapUserModelToDao(User user)
        {
            return new UserDao
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                Password = user.Password,
                LastName = user.LastName,
                Username = user.Username,
                Role = (int)user.Role
            };
        }
        #endregion
    }
}