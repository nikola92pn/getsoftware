﻿using ProjectDao = GetSoftware.DAL.Models.Project;
using Project = GetSoftware.Infrastructure.Models.Project;
using GetSoftware.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System;

namespace GetSoftware.Infrastructure.Managers
{
    public class ProjectManager
    {
        private readonly IRepository<ProjectDao> repository;
        
        #region Ctor

        /// <summary>
        /// Creates an instance of ProjectManager
        /// </summary>
        /// <param name="_repository"></param>
        public ProjectManager(IRepository<ProjectDao> _repository)
        {
            repository = _repository;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets all projects
        /// </summary>
        /// <returns></returns>
        public List<Project> GetAllProjects()
        {
            List<ProjectDao> projects = repository.GetAll();

            return projects.Select(p => new Project(p)).ToList();
        }

        /// <summary>
        /// Gets project by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Project GetProjectById(int id)
        {
            ProjectDao project = repository.GetById(id);

            return project != null ? new Project(project) : null;
        }

        /// <summary>
        /// Updates an instance of Project
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public bool UpdateProject(Project project)
        {
            ProjectDao projectDao = MapProjectModelToDao(project);

            return repository.Edit(projectDao);
        }

        /// <summary>
        /// Inserts an instance of Project
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public bool InsertProject(Project project)
        {
            ProjectDao projectDao = MapProjectModelToDao(project);

            return repository.Add(projectDao);
        }

        /// <summary>
        /// Remove an instance of Project
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool RemoveProject(int id)
        {
            return repository.Remove(id);
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Maps Project model to DAO object
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        private ProjectDao MapProjectModelToDao(Project project)
        {
            return new ProjectDao
            {
                Id = project.Id,
                Code = project.Code,
                Name = project.Name,
                ProjectManagerId = project.ProjectManager.Id
            };
        }
        #endregion
    }
}
