﻿using GetSoftware.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace GetSoftware.Infrastructure.Models
{
    public class User
    {
        /// <summary>
        /// The id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Unique username
        /// </summary>
        [Required]
        public string Username { get; set; }

        /// <summary>
        /// The password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The email address
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// The first name
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// The role type
        /// </summary>
        public RoleType Role { get; set; }

        #region Ctors
        public User(DAL.Models.User dao)
        {
            Id = dao.Id;
            Username = dao.Username;
            Password = dao.Password;
            Email = dao.Email;
            FirstName = dao.FirstName;
            LastName = dao.LastName;
            Role = (RoleType)dao.Role;
        }

        public User()
        {

        }
        #endregion
    }
}
