﻿namespace GetSoftware.Infrastructure.Models
{
    /// <summary>
    /// Task status types
    /// </summary>
    public enum StatusType
    {
        NEW = 0,
        IN_PROGRESS = 1,
        FINISHED = 2
    }
}