﻿namespace GetSoftware.Infrastructure.Models
{
    /// <summary>
    /// User role types
    /// </summary>
    public enum RoleType
    {
        ADMINISTRATOR = 0,
        PROJECT_MANAGER = 1,
        DEVELOPER = 2
    }
}