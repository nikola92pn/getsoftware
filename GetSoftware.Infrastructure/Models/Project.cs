﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectDao = GetSoftware.DAL.Models.Project;
using TaskModel = GetSoftware.Infrastructure.Models.Task;

namespace GetSoftware.Infrastructure.Models
{
    public class Project
    {
        /// <summary>
        /// The id
        /// </summary>        
        public int Id { get; set; }

        /// <summary>
        /// The project name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The unique project code
        /// </summary>
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// The average progress depending on tasks progress
        /// </summary>
        public int Progress { get; set; }

        /// <summary>
        /// Instance of ProjectManagers
        /// </summary>
        public User ProjectManager { get; set; }

        /// <summary>
        /// Collection of tasks that belongs to the project
        /// </summary>
        public List<TaskModel> Tasks { get; set; }

        #region Ctors
        public Project(ProjectDao dao)
        {
            Id = dao.Id;
            Name = dao.Name;
            Code = dao.Code;
            ProjectManager = new User(dao.ProjectManager);
            LoadTasksAndProgress(dao);
        }

        public Project()
        {
        }
        #endregion

        #region Private methods
        private void LoadTasksAndProgress(ProjectDao dao)
        {
            Tasks = dao.Tasks?.Select(t => new TaskModel(t, this)).ToList();
            Progress = Tasks == null || Tasks.Count == 0 ? 0 : Convert.ToInt32(Tasks.Average(t => t.Progress));
        }
        #endregion
    }
}