﻿using System;
using System.ComponentModel.DataAnnotations;
using TaskDao = GetSoftware.DAL.Models.Task;

namespace GetSoftware.Infrastructure.Models
{
    public class Task
    {
        /// <summary>
        /// The id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Instance of assignee
        /// </summary>
        public User Assingee { get; set; }

        /// <summary>
        /// Instance of project
        /// </summary>
        public Project Project { get; set; }

        /// <summary>
        /// The status
        /// </summary>
        [Required]
        public StatusType Status { get; set; }

        /// <summary>
        /// The progress
        /// </summary>
        [Required]
        [Range(0, 100)]
        public int Progress { get; set; }

        /// <summary>
        /// The deadline
        /// </summary>
        public string Deadline { get; set; }

        /// <summary>
        /// The description
        /// </summary>
        public string Description { get; set; }

        #region Ctors
        public Task(TaskDao dao, Project project)
        {
            Id = dao.Id;
            Assingee = dao.Assignee != null ? new User(dao.Assignee) : null;
            Project = project;
            Status = (StatusType)dao.Status;
            Progress = dao.Progress;
            Deadline = dao.Deadline.HasValue ? dao.Deadline.Value.ToString("yyyy-MM-ddTHH:mm") : null;
            Description = dao.Description;
        }

        public Task(TaskDao dao)
        {
            Id = dao.Id;
            Assingee = dao.Assignee != null ? new User(dao.Assignee) : null;
            Project = new Project(dao.Project);
            Status = (StatusType)dao.Status;
            Progress = dao.Progress;
            Deadline = dao.Deadline.HasValue ? dao.Deadline.Value.ToString("yyyy-MM-ddTHH:mm") : null;
            Description = dao.Description;
        }

        public Task()
        {

        }
        #endregion
    }
}
