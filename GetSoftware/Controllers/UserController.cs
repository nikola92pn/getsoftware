﻿using GetSoftware.Infrastructure.Managers;
using GetSoftware.Infrastructure.Models;
using System.Web.Mvc;

namespace GetSoftware.Controllers
{
    public class UserController : BaseController
    {
        #region Private fields
        private UserManager userManager;
        #endregion

        /// <summary>
        /// Creates an instance of UserController
        /// </summary>
        public UserController()
        {
            userManager = new UserManager(userRepository, taskRepository);
        }

        /// <summary>
        /// Gets view with all users
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View("Index", userManager.GetAllUsers());
        }

        /// <summary>
        /// Gets view for specific user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ViewUser(int id)
        {
            if (id != 0)
            {
                return View("User", userManager.GetUserById(id));
            }
            else
            {
                return View("User", new User());
            }
        }

        /// <summary>
        /// Updates or inserts user and return to list
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ActionResult UpsertUser(User user)
        {
            ValidateModel(user);

            bool success = false;

            if (user.Id != 0)
            {
                success = userManager.UpdateUser(user);
            }
            else
            {
                success = userManager.InsertUser(user);
            }

            if (success)
            {
                ViewBag.Message = "Success";
            }
            else
            {
                ViewBag.ErrorMessage = "Something went wrong while saving the user!";
            }

            return Index();
        }

        /// <summary>
        /// Removes user and return to list
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ActionResult RemoveUser(User user)
        {
            bool success = userManager.RemoveUser(user.Id);

            if (success)
            {
                ViewBag.Message = "Success";
            }
            else
            {
                ViewBag.ErrorMessage = "Something went wrong while deleting the user!";
            }

            return Index();
        }

    }
}