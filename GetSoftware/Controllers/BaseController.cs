﻿using GetSoftware.DAL.Repositories;
using GetSoftware.Infrastructure.Models;
using System.Web.Mvc;
using System.Web.Routing;
using DAO = GetSoftware.DAL.Models;

namespace GetSoftware.Controllers
{
    public class BaseController : Controller
    {
        #region Repositories binding
        protected readonly IRepository<DAO.Project> projectRepository = DependencyResolver.Current.GetService<IRepository<DAO.Project>>();
        protected readonly IRepository<DAO.User> userRepository = DependencyResolver.Current.GetService<IRepository<DAO.User>>();
        protected readonly IRepository<DAO.Task> taskRepository = DependencyResolver.Current.GetService<IRepository<DAO.Task>>();
        #endregion

        /// <summary>
        /// This method will simulates user's authorization
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            User user = (User)Session["User"];
            if (user == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                                new
                                {
                                    controller = "Login",
                                    action = "Index"
                                }));
            }

            base.OnActionExecuted(filterContext);
        }
    }
}