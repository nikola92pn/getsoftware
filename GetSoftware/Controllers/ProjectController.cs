﻿using GetSoftware.DAL.Repositories;
using GetSoftware.Infrastructure.Managers;
using GetSoftware.Infrastructure.Models;
using DAO = GetSoftware.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GetSoftware.Controllers
{
    public class ProjectController : BaseController
    {
        #region Private fields
        private ProjectManager projectManager;
        private UserManager userManager;
        #endregion

        /// <summary>
        /// Creates an instance of ProjectController with managers
        /// </summary>
        public ProjectController()
        {
            projectManager = new ProjectManager(projectRepository);
            userManager = new UserManager(userRepository, taskRepository);
        }
        
        /// <summary>
        /// Gets view with list of projects
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.CreateEnable = userManager.GetAllProjectManagers().Count > 0;
            return View("Index", projectManager.GetAllProjects());
        }

        /// <summary>
        /// Gets view for specific project
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ViewProject(int id)
        {
            PrepareDDList();

            if (id != 0)
            {
                return View("Project", projectManager.GetProjectById(id));
            }
            else
            {
                return View("Project", new Project());
            }
        }

        /// <summary>
        /// Updates or inserts an instance of project 
        /// and returns view with project lists
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public ActionResult UpsertProject(Project project)
        {            
            LoadProjectManager(ref project);

            bool success = false;

            if (project.Id != 0)
            {
                success = projectManager.UpdateProject(project);
            }
            else
            {
                success = projectManager.InsertProject(project);
            }

            if (success)
            {
                ViewBag.Message = "Success";
            }
            else
            {
                ViewBag.ErrorMessage = "Something went wrong while saving the project!";
            }

            return Index();
        }

        /// <summary>
        /// Removes an instance of project and return to list
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public ActionResult RemoveProject(Project project)
        {
            bool success = projectManager.RemoveProject(project.Id);

            if (success)
            {
                ViewBag.Message = "Success";
            }
            else
            {
                ViewBag.ErrorMessage = "Something went wrong while deleting the project!";
            }

            return Index();
        }

        #region Private methods
        private void LoadProjectManager(ref Project project)
        {
            User loggedUser = Session["User"] as User;
            if (project.ProjectManager == null)
            {
                project.ProjectManager = loggedUser;
            }
            else
            {
                project.ProjectManager = userManager.GetUserById(project.ProjectManager.Id);                
            }
        }

        private void PrepareDDList()
        {
            if (((User)Session["User"]).Role == RoleType.ADMINISTRATOR)
            {
                ViewBag.EnableDDPM = true;

                List<User> users = userManager.GetAllUsers();

                List<User> managers = users.Where(u => u.Role == RoleType.PROJECT_MANAGER).ToList();
                if(managers.Count == 0)
                {
                    throw new InvalidProgramException("There is no project managers in the system.");
                }

                ViewBag.UsersDD = managers;
            }
            else
            {
                ViewBag.EnableDDPM = false;
            }
        }
        #endregion
    }
}