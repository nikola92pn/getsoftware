﻿using GetSoftware.DAL.Repositories;
using DAO = GetSoftware.DAL.Models;
using GetSoftware.Infrastructure.Managers;
using GetSoftware.Infrastructure.Models;
using System.Web.Mvc;

namespace GetSoftware.Controllers
{
    public class LoginController : Controller
    {
        protected readonly IRepository<DAO.User> userRepository = DependencyResolver.Current.GetService<IRepository<DAO.User>>();
        protected readonly IRepository<DAO.Task> taskRepository = DependencyResolver.Current.GetService<IRepository<DAO.Task>>();

        /// <summary>
        /// Goes to login page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return PartialView("Login");
        }

        /// <summary>
        /// Login user to the system
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            // simulates login method, it should be more complex then just getting user
            User user = new UserManager(userRepository, taskRepository)
                            .GetUser(username, password);

            if (user == null)
            {
                ViewBag.Message = "The username or password provided is incorrect.";
                return PartialView("Login");
            }

            Session["User"] = user;

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Simulates logout from system
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session["User"] = null;
            return View("Login");
        }
    }
}