﻿using GetSoftware.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GetSoftware.Controllers
{
    public class HomeController : BaseController
    {
        /// <summary>
        /// Returns to valid page depending on user session
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            User user = (User)Session["User"];
            if (user != null)
            {
                return View();
            }
            return PartialView("Login");
        }
        
    }
}