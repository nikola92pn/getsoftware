﻿using GetSoftware.Infrastructure.Managers;
using GetSoftware.Infrastructure.Models;
using User = GetSoftware.Infrastructure.Models.User;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GetSoftware.DAL.Repositories;

namespace GetSoftware.Controllers
{
    public class TaskController : BaseController
    {
        #region Private fields
        private ProjectManager projectManager;
        private TaskManager taskManager;
        private UserManager userManager;
        private List<Project> allProjects;
        #endregion

        /// <summary>
        /// Creates an instance of TaskController
        /// </summary>
        public TaskController()
        {
            projectManager = new ProjectManager(projectRepository);
            taskManager = new TaskManager(taskRepository);
            userManager = new UserManager(userRepository, taskRepository);
            allProjects = projectManager.GetAllProjects();
        }
        
        /// <summary>
        /// Gets all task relevant for the user
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            SetCreateButtonVisibility();

            List<Task> allTasks = null;

            User loggedInUser = ((User)Session["User"]);
            if (loggedInUser.Role == RoleType.DEVELOPER)
            {
                allTasks = taskManager.GetAllTasks()
                    .Where(t => t.Assingee == null || t.Assingee.Id == loggedInUser.Id)
                    .ToList();
            }
            else
            {
                allTasks = taskManager.GetAllTasks();
            }

            return View("Index", allTasks);
        }
        
        /// <summary>
        /// Returns task view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ViewTask(int id)
        {
            Task task = new Task();

            if (id != 0)
            {                
                task = taskManager.GetTaskForEditing(id, (User)Session["User"]);

                if (task == null)
                {
                    ViewBag.WarningMessage = "User can modified only tasks that are assigned to the user.";
                    return Index();
                }
            }

            PrepareDropDownLists(task);

            return View("Task", task);
        }
        
        /// <summary>
        /// Updates or inserts an instance of tast and returns to list
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public ActionResult UpsertTask(Task task)
        {
            LoadProjectAndUserFromDD(ref task);

            bool success = false;

            if (task.Id != 0)
            {
                success = taskManager.UpdateTaks(task);
            }
            else
            {
                success = taskManager.InsertTask(task);
            }

            if (success)
            {
                ViewBag.Message = "Success";
            }
            else
            {
                ViewBag.ErrorMessage = "Something went wrong while saving the task!";
            }

            return Index();
        }

        /// <summary>
        /// Removes tasks and return to list
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public ActionResult RemoveTask(Task task)
        {
            bool success = taskManager.RemoveTask(task.Id);

            if (success)
            {
                ViewBag.Message = "Success";
            }
            else
            {
                ViewBag.ErrorMessage = "Something went wrong while deleting the task!";
            }

            return Index();
        }

        #region Private methods
        private void LoadProjectAndUserFromDD(ref Task task)
        {
            int idP = task.Project.Id;
            task.Project = allProjects.FirstOrDefault(p => p.Id == idP);

            if (task.Assingee != null)
            {
                int idU = task.Assingee.Id;
                task.Assingee = userManager.GetUserById(idU);
            }
        }

        private void SetCreateButtonVisibility()
        {
            ViewBag.CreateEnable = allProjects.Count > 0 && ((User)Session["User"]).Role != RoleType.DEVELOPER;
        }

        private void PrepareDropDownLists(Task task)
        {
            ViewBag.ProjectsDD = projectManager.GetAllProjects();
            List<User> allUsers;

            if (((User)Session["User"]).Role == RoleType.PROJECT_MANAGER)
            {
                allUsers = userManager.GetAllAvailableDevelopers();
            }
            else
            {
                allUsers = userManager.GetAllAvailableUsers();
            }

            allUsers.Insert(0, new User());
            User assignee = task?.Assingee;
            if(assignee != null && 
                allUsers.FirstOrDefault(u => u.Id == assignee.Id) == null)
            {
                allUsers.Add(assignee);
            }

            ViewBag.UsersDD = allUsers;
        }
        #endregion
    }
}