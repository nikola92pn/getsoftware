﻿using GetSoftware.DAL.Models;
using GetSoftware.DAL.Repositories;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GetSoftware.App_Start
{
    /// <summary>
    /// Instance of DependencyResolver
    /// </summary>
    public class NinjectResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectResolver()
        {
            _kernel = new StandardKernel();
            AddBindings();
        }
        
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IRepository<User>>().To<UserRepository>();
            _kernel.Bind<IRepository<Project>>().To<ProjectRepository>();
            _kernel.Bind<IRepository<Task>>().To<TaskRepository>();
        }
    }
}