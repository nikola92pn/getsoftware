﻿using System;
using System.Collections.Generic;
using System.Linq;
using GetSoftware.DAL.Models;
using GetSoftware.DAL.Repositories;

namespace GetSofware.Tests
{
    internal class MockTaskRepository : IRepository<Task>
    {
        public bool Add(Task n)
        {
            throw new System.NotImplementedException();
        }

        public bool Edit(Task n)
        {
            throw new System.NotImplementedException();
        }

        public List<Task> GetAll()
        {
            return new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 2,
                    Assignee = new MockUserRepository().GetAll().First(u => u.Id == 2),
                    Description = "First task",
                    Progress = 0,
                    Status = 0,
                    Project = new MockProjectRepository().GetAll().First()
                },
                new Task
                {
                    Id = 2,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 2,
                    Assignee = new MockUserRepository().GetAll().First(u => u.Id == 2),
                    Description = "second task",
                    Progress = 0,
                    Status = 0,
                    Project = new MockProjectRepository().GetAll().First()
                },
                new Task
                {
                    Id = 3,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 2,
                    Assignee = new MockUserRepository().GetAll().First(u => u.Id == 2),
                    Description = "third task",
                    Progress = 0,
                    Status = 0,
                    Project = new MockProjectRepository().GetAll().First()
                },
                new Task
                {
                    Id = 4,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 1,
                    Assignee = new MockUserRepository().GetAll().First(u => u.Id == 1),
                    Description = "fourh task",
                    Progress = 0,
                    Status = 0,
                    Project = new MockProjectRepository().GetAll().First()
                }
            };
        }

        public Task GetById(int id)
        {
            return GetAll().FirstOrDefault(t => t.Id == id);
        }

        public bool Remove(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}