﻿using GetSoftware.DAL.Models;
using GetSoftware.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetSofware.Tests
{
    public class MockProjectRepository : IRepository<Project>
    {
        public bool Add(Project n)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Project n)
        {
            throw new NotImplementedException();
        }

        public List<Project> GetAll()
        {
            return new List<Project>
            {
                new Project
                {
                    Id = 1,
                    Name = "Main project",
                    Code = "mmm",
                    ProjectManagerId = 1,
                    ProjectManager = new MockUserRepository().GetAll().First()                       
                }
            };
        }

        public Project GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
