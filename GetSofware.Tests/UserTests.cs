﻿using DAO = GetSoftware.DAL.Models;
using GetSoftware.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetSoftware.Infrastructure.Managers;
using GetSoftware.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace GetSofware.Tests
{
    [TestClass]
    public class UserTests
    {
        private static IRepository<DAO.User> userRepository;
        private static IRepository<DAO.Task> taskRepository;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            userRepository = new MockUserRepository();
            taskRepository = new MockTaskRepository();
        }

        [TestMethod]
        public void GetAvailableDevelopers()
        {
            UserManager userManager = new UserManager(userRepository, taskRepository);

            List<User> avDevs = userManager.GetAllAvailableDevelopers();

            Assert.IsTrue(avDevs.Count == 1);
            Assert.IsTrue(avDevs.First().Id == 1);
        }        
    }
}
