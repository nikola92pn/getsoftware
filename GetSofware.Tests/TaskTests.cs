﻿using GetSoftware.DAL.Repositories;
using DAO = GetSoftware.DAL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetSoftware.Infrastructure.Managers;
using GetSoftware.Infrastructure.Models;

namespace GetSofware.Tests
{
    [TestClass]
    public class TaskTests
    {
        private static IRepository<DAO.User> userRepository;
        private static IRepository<DAO.Task> taskRepository;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            userRepository = new MockUserRepository();
            taskRepository = new MockTaskRepository();
        }

        [TestMethod]
        public void CanUserEditTask()
        {
            UserManager userManager = new UserManager(userRepository, taskRepository);
            TaskManager taskManager = new TaskManager(taskRepository);

            User user = userManager.GetUserById(2);

            Task allowedTask = taskManager.GetTaskForEditing(1, user);
            Assert.IsNotNull(allowedTask);

            Task disallowedTask = taskManager.GetTaskForEditing(4, user);
            Assert.IsNull(disallowedTask);
        }
    }
}
