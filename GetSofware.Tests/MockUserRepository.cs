﻿using System.Collections.Generic;
using System.Linq;
using GetSoftware.DAL.Models;
using GetSoftware.DAL.Repositories;

namespace GetSofware.Tests
{
    internal class MockUserRepository : IRepository<User>
    {
        public bool Add(User n)
        {
            throw new System.NotImplementedException();
        }

        public bool Edit(User n)
        {
            throw new System.NotImplementedException();
        }

        public List<User> GetAll()
        {
            return new List<User> {
                new User
                {
                    Id = 1,
                    Email = "petar@mail.com",
                    FirstName = "Petar",
                    LastName = "Peric",
                    Username = "petar",
                    Password = "petar",
                    Role = 2
                },
                new User
                {
                    Id = 2,
                    Email = "petar2@mail.com",
                    FirstName = "Petar2",
                    LastName = "Peric2",
                    Username = "petar2",
                    Password = "petar2",
                    Role = 2
                },
                new User
                {
                    Id = 3,
                    Email = "mng@mail.com",
                    FirstName = "Manager",
                    LastName = "Manager",
                    Username = "manager",
                    Password = "pwd",
                    Role = 1
                }};
        }

        public User GetById(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public bool Remove(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}