﻿using GetSoftware.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity;
using System.Linq;

namespace GetSoftware.DAL.Repositories
{
    /// <summary>
    /// The repository interface with CRUD operation on the table
    /// </summary>
    public class ProjectRepository : IRepository<Project>
    {
        /// <summary>
        /// Add new instance of news to the table
        /// </summary>
        /// <param name="n"></param>
        public bool Add(Project n)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    context.ProjectSet.Add(n);
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }

        /// <summary>
        /// Edits existing news
        /// </summary>
        /// <param name="n"></param>
        public bool Edit(Project n)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    context.ProjectSet.Attach(n);
                    context.Entry(n).State = EntityState.Modified;
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }

        /// <summary>
        /// Gets all news from table
        /// </summary>
        /// <returns></returns>
        public List<Project> GetAll()
        {
            List<Project> result = new List<Project>();
            try
            {
                using (GetContext context = new GetContext())
                {
                    result = context.ProjectSet
                        .Include(p => p.Tasks.Select(t => t.Assignee))
                        .Include(p => p.ProjectManager)
                        .ToList();

                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return result;
        }

        /// <summary>
        /// Gets news by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Project GetById(int id)
        {
            Project result = null;
            try
            {
                using (GetContext context = new GetContext())
                {
                    result = context.ProjectSet
                        .Include(p => p.Tasks.Select(t => t.Assignee))
                        .Include(p => p.ProjectManager)
                        .FirstOrDefault(u => u.Id == id);
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return result;
        }

        /// <summary>
        /// Remove news from table for specific ID
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(int id)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    Project projectForDeleting = new Project
                    {
                        Id = id
                    };

                    context.ProjectSet.Attach(projectForDeleting);
                    context.ProjectSet.Remove(projectForDeleting);
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }
    }
}