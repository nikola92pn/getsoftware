﻿using System.Collections.Generic;

namespace GetSoftware.DAL.Repositories
{
    /// <summary>
    /// The repository interface with CRUD operation on the table
    /// </summary>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets all news from table
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();

        /// <summary>
        /// Gets news by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        /// <summary>
        /// Add new instance of news to the table
        /// </summary>
        /// <param name="n"></param>
        bool Add(T n);

        /// <summary>
        /// Edits existing news
        /// </summary>
        /// <param name="n"></param>
        bool Edit(T n);

        /// <summary>
        /// Remove news from table for specific ID
        /// </summary>
        /// <param name="id"></param>
        bool Remove(int id);
    }
}
