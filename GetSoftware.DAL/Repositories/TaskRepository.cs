﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using GetSoftware.DAL.Models;

namespace GetSoftware.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {

        /// <summary>
        /// Add new instance of news to the table
        /// </summary>
        /// <param name="n"></param>
        public bool Add(Task n)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    context.TaskSet.Add(n);
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }

        /// <summary>
        /// Edits existing news
        /// </summary>
        /// <param name="n"></param>
        public bool Edit(Task n)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    context.TaskSet.Attach(n);
                    context.Entry(n).State = EntityState.Modified;
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }

        /// <summary>
        /// Gets all news from table
        /// </summary>
        /// <returns></returns>
        public List<Task> GetAll()
        {
            List<Task> result = new List<Task>();
            try
            {
                using (GetContext context = new GetContext())
                {
                    result = context.TaskSet
                        .Include(t => t.Assignee)
                        .Include(t => t.Project)
                        .Include(t => t.Project.ProjectManager)
                        .ToList();
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return result;
        }

        /// <summary>
        /// Gets news by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task GetById(int id)
        {
            Task result = null;
            try
            {
                using (GetContext context = new GetContext())
                {
                    result = context.TaskSet
                        .Include(t => t.Assignee)
                        .Include(t => t.Project)
                        .Include(t => t.Project.ProjectManager)
                        .FirstOrDefault(u => u.Id == id);
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return result;
        }

        /// <summary>
        /// Remove news from table for specific ID
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(int id)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    Task taskForDeleting = new Task
                    {
                        Id = id
                    };
                    context.TaskSet.Attach(taskForDeleting);
                    context.TaskSet.Remove(taskForDeleting);
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }
    }
}
