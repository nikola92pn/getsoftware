﻿using GetSoftware.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;

namespace GetSoftware.DAL.Repositories
{
    /// <summary>
    /// The repository interface with CRUD operation on the table
    /// </summary>
    public class UserRepository : IRepository<User>
    {

        /// <summary>
        /// Add new instance of news to the table
        /// </summary>
        /// <param name="n"></param>
        public bool Add(User n)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    context.UserSet.Add(n);
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }

        /// <summary>
        /// Edits existing news
        /// </summary>
        /// <param name="n"></param>
        public bool Edit(User n)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    context.UserSet.Attach(n);
                    context.Entry(n).State = EntityState.Modified;
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }

        /// <summary>
        /// Gets all news from table
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            List<User> result = new List<User>();
            try
            {
                using (GetContext context = new GetContext())
                {
                    result = context.UserSet.ToList();
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return result;
        }

        /// <summary>
        /// Gets news by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetById(int id)
        {
            User result = null;
            try
            {
                using (GetContext context = new GetContext())
                {
                    result = context.UserSet.FirstOrDefault(u => u.Id == id);
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return result;
        }


        /// <summary>
        /// Remove news from table for specific ID
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(int id)
        {
            try
            {
                using (GetContext context = new GetContext())
                {
                    User userForDeleting = new User
                    {
                        Id = id
                    };

                    context.UserSet.Attach(userForDeleting);
                    context.UserSet.Remove(userForDeleting);
                    return context.SaveChanges() > 0;
                }
            }
            catch (EntityException eex)
            {
                Console.WriteLine(eex);
            }
            return false;
        }
    }
}
