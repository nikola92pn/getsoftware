﻿using GetSoftware.DAL.Models;
using System;
using System.Data.Entity;

namespace GetSoftware.DAL
{
    class GetDBInitialize : DropCreateDatabaseIfModelChanges<GetContext>
    {

        protected override void Seed(GetContext context)
        {
            context.UserSet.Add(
                new User
                {
                    Id = 1,
                    Email = "nikola@mail.com",
                    FirstName = "Nikola",
                    LastName = "Todorovic",
                    Username = "nikola",
                    Password = "nikola",
                    Role = 0
                });

            context.UserSet.Add(
                new User
                {
                    Id = 2,
                    Email = "marko@mail.com",
                    FirstName = "Marko",
                    LastName = "Markovic",
                    Username = "marko",
                    Password = "marko",
                    Role = 1
                });

            context.UserSet.Add(
                new User
                {
                    Id = 3,
                    Email = "petar@mail.com",
                    FirstName = "Petar",
                    LastName = "Peric",
                    Username = "petar",
                    Password = "petar",
                    Role = 2
                });

            context.ProjectSet.Add(
                new Project
                {
                    Id = 1,
                    Name = "Main project",
                    Code = "mmm",
                    ProjectManagerId = 2
                });

            context.TaskSet.Add(
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 3,
                    Description = "First task",
                    Progress = 0,
                    Status = 0
                });

            context.TaskSet.Add(
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 3,
                    Description = "Second task",
                    Progress = 50,
                    Status = 2
                });

            context.TaskSet.Add(
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    Deadline = DateTime.Now.AddDays(300),
                    AssigneeId = 3,
                    Description = "Third task",
                    Progress = 100,
                    Status = 3
                });


            context.SaveChanges();
            base.Seed(context);
        }
    }
}
