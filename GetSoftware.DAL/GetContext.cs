﻿using GetSoftware.DAL.Models;
using System.Configuration;
using System.Data.Entity;

namespace GetSoftware.DAL
{
    /// <summary>
    /// Instance of Dbcontext object
    /// </summary>
    class GetContext : DbContext
    {
        /// <summary>
        /// Contructor for GetContext
        /// </summary>
        public GetContext() : base(ConfigurationManager.ConnectionStrings["getDB"].ConnectionString)
        {
            // it is a CODE FIRST APPROACH hence it will create init db data
            Database.SetInitializer<GetContext>(new GetDBInitialize());
        }

        public DbSet<User> UserSet { get; set; }
        public DbSet<Project> ProjectSet { get; set; }
        public DbSet<Task> TaskSet { get; set; }
    }
}
