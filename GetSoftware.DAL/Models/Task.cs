﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GetSoftware.DAL.Models
{
    public class Task
    {
        /// <summary>
        /// The id
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// The assignee id
        /// </summary>
        public int? AssigneeId { get; set; }

        /// <summary>
        /// The project id
        /// </summary>
        [Required]
        public int ProjectId { get; set; }
        
        /// <summary>
        /// The status
        /// </summary>
        [Required]
        public int Status { get; set; }

        /// <summary>
        /// The progress
        /// </summary>
        [Range(0, 100)]
        public int Progress { get; set; }

        /// <summary>
        /// The deadline
        /// </summary>
        public DateTime? Deadline { get; set; }

        /// <summary>
        /// The description
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Instance of assignee
        /// </summary>
        [ForeignKey("AssigneeId")]
        public User Assignee { get; set; }

        /// <summary>
        /// Instance of project
        /// </summary>
        [ForeignKey("ProjectId")]
        public Project Project { get; set; }

    }
}
