﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetSoftware.DAL.Models
{
    public class Project
    {
        /// <summary>
        /// The id
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// The project name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The unique project code
        /// </summary>
        [Index(IsUnique = true)]
        [StringLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets id of project manager
        /// </summary>
        [Required]
        public int ProjectManagerId { get; set; }

        /// <summary>
        /// Instance of ProjectManagers
        /// </summary>
        [ForeignKey("ProjectManagerId")]
        public User ProjectManager { get; set; }

        /// <summary>
        /// Collection of tasks that belongs to the project
        /// </summary>
        public ICollection<Task> Tasks { get; set; }
    }
}
